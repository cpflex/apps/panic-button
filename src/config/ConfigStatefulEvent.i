/**
*  Name:  ConfigSystem.p
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2022 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for the battery module.
**/

/*************************
* Battery Configuration 
**************************/
#include <StatefulEventDefs.i>
#include <OcmNidDefinitions.i>

const SE_DEF_STATECOUNT = 1;

//FIELDS ARE nidMsg, flags, nidSet, nidClear, priority, category, initialState
stock SE_EVENTDEFS[SE_DEF_STATECOUNT][.nidMsg, .flags, .nidSet, .nidClear, .priority, .category, .initialState] = [
	//Emergency Mode.
	[ 
		_: NID_PbDuress, 
		_: SE_CONFIG_PERSIST_STATE, 
		_: NID_PbDuressActivated, 
		_: NID_PbDuressDeactivated, 
		_: MP_critical,
		_: MC_alert,
		_: SE_CLEAR
	]
];


