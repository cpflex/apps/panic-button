/*******************************************************************************
*                !!! OCM CODE GENERATED FILE -- DO NOT EDIT !!!
*
* ODS Panic Button Schema
*  nid:        PanicButton
*  uuid:       8865ed6e-cc61-4f9f-b0de-7555fa2939cb
*  ver:        1.0.0.0
*  date:       2023-08-04T23:35:00.377Z
*  product: Cora Tracking CT100X
* 
*  Copyright 2023 - Codepoint Technologies, Inc. All Rights Reserved.
* 
*******************************************************************************/
const OcmNidDefinitions: {
	NID_PowerCharger = 1,
	NID_SystemLogAll = 2,
	NID_SystemErase = 3,
	NID_PowerChargerCharging = 4,
	NID_SystemLogDisable = 5,
	NID_SystemLogInfo = 6,
	NID_PowerBattery = 7,
	NID_SystemReset = 8,
	NID_PowerChargerCritical = 9,
	NID_SystemConfigPollintvl = 10,
	NID_SystemLog = 11,
	NID_SystemLogAlert = 12,
	NID_SystemLogDetail = 13,
	NID_PowerChargerCharged = 14,
	NID_SystemConfig = 15,
	NID_PbDuressActivated = 16,
	NID_PbDuress = 17,
	NID_PbDuressDeactivated = 18,
	NID_PowerChargerDischarging = 19
};
