"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Run = void 0;
async function Run(o, _device) {
  // The raw data received from the device. Should not be edited.
  const orgData = o.data.original.data;

  let nid = "";
  if (typeof orgData.nid !== "undefined") {
    nid = orgData.nid;
  }

  switch (nid) {
    case "PB.Duress": {
      if (orgData.svals[0] === "PB.Duress.Activated") {
        const notificationSetting = {
          body:
            _device.name +
            " device panic button has been activated. Response is required.",
          // eslint-disable-next-line max-len
          icon: "https://s3.eu-west-1.amazonaws.com/v3.thinxmate.com/pictures/5968e243-1538-49d4-976b-dac3cd8e6687-alert-512.png",
          title: "Panic Button Activated",
        };
        await sharing.alarm.insert(
          {
            ...o,
            deviceId: _device.id,
            device: _device,
            ruleId: _rule.id,
            userId: _device.userId || 0,
          },
          orgData?.rule?.json.message || "Panic Button Activated",
          notificationSetting
        );
        _device.signals.set("Mode", "Emergency Mode Activated");
        _device.signals.set(
          "Status",
          "Device panic button pressed.  Response is required."
        );
      } else if (orgData.svals[0] === "PB.Duress.Deactivated") {
        await sharing.alarm.dissolve(_device.id, _rule.id, o);
        _device.signals.set("Mode", "Emergency Mode Cancelled");
        _device.signals.set(
          "Status",
          "Emergency mode cancelled by device user.  No further response required."
        );
      }
      break;
    }
  }
}
exports.Run = Run;
