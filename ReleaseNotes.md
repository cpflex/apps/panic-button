﻿# Panic Button Application Release Notes

### V1.004a - 231206
1. Updated CT1XXX firmware reference to v2.4.4.2a
 - Fixed TimeSync bug (KP-350)
 - Various RTC Kernel Fixes
 - NETWORK RESET operation now rejoins instead of clearing NVM params
 - DUX12 accelerometer bug fixes
2. LoraWAN v1.04 radio stack
  - US915 SB2 / ADR Disabled
  - EU868 Standard Band / ADR enabled

### V1.003 - 230919
1. Firmware Reference v2.3.0.0
   * Incorporates all stabilization improvements.  This is a nearly final release candidate for the firmware.
2. Flex Platform: v1.4.0.2
3. Standard EU band ADR enabled.

### V1.002 - 230808
  1. Updated Platform  v1.3.2.2
     * Bugfix:  fixed duplicate state issue.
### V1.001 - 230806
  1. Debugged initial version.
  
### V1.000a - 230804
  1. Firmware Reference  v2.2.1.0
  2. Initial Implementation
